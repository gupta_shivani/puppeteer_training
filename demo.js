const puppeteer = require('puppeteer');
const expect = require('chai').expect;

const runPuppeteer = async () => {
    const browser = await puppeteer.launch({ headless: false });
    const page = await browser.newPage();
    await page.setDefaultTimeout(10000);
    await page.goto('https://www.google.com');
    await page.type('[class="gLFyf gsfi"]', 'puppeteer');
    await page.keyboard.press('Enter');
    //await page.click('[class="gNO89b"]');
    await page.waitFor(3000);
    const text = await page.evaluate(() => document.querySelector('[class="LC20lb"]').innerText);
    await page.screenshot({ path: './exmaple.png' });
    expect(text).to.contain('puppeteer');
    await page.close();
    await browser.close();
};

runPuppeteer();